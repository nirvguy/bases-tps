--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE IF EXISTS ONLY public."Pago" DROP CONSTRAINT IF EXISTS fk_utiliza;
ALTER TABLE IF EXISTS ONLY public."Evento" DROP CONSTRAINT IF EXISTS fk_ubicado;
ALTER TABLE IF EXISTS ONLY public."Direccion" DROP CONSTRAINT IF EXISTS fk_ubicado;
ALTER TABLE IF EXISTS ONLY public."Provincia" DROP CONSTRAINT IF EXISTS fk_ubicado;
ALTER TABLE IF EXISTS ONLY public."Empresa" DROP CONSTRAINT IF EXISTS fk_ubicada;
ALTER TABLE IF EXISTS ONLY public."tieneCategoria" DROP CONSTRAINT IF EXISTS fk_tiene_tarjeta;
ALTER TABLE IF EXISTS ONLY public."tieneCategoria" DROP CONSTRAINT IF EXISTS fk_tiene_categoria;
ALTER TABLE IF EXISTS ONLY public."Factura" DROP CONSTRAINT IF EXISTS fk_tarjeta;
ALTER TABLE IF EXISTS ONLY public.sucede DROP CONSTRAINT IF EXISTS fk_sucede_evento;
ALTER TABLE IF EXISTS ONLY public.sucede DROP CONSTRAINT IF EXISTS fk_sucede_dia;
ALTER TABLE IF EXISTS ONLY public."registraMedioDePago" DROP CONSTRAINT IF EXISTS fk_registra_usuario;
ALTER TABLE IF EXISTS ONLY public."registraAtraccion" DROP CONSTRAINT IF EXISTS fk_registra_tarjeta;
ALTER TABLE IF EXISTS ONLY public."registraMedioDePago" DROP CONSTRAINT IF EXISTS "fk_registra_medioDePago";
ALTER TABLE IF EXISTS ONLY public."registraAtraccion" DROP CONSTRAINT IF EXISTS fk_registra_atraccion;
ALTER TABLE IF EXISTS ONLY public."Usuario" DROP CONSTRAINT IF EXISTS fk_pertenece;
ALTER TABLE IF EXISTS ONLY public."Pago" DROP CONSTRAINT IF EXISTS fk_pago;
ALTER TABLE IF EXISTS ONLY public."Evento" DROP CONSTRAINT IF EXISTS fk_organiza;
ALTER TABLE IF EXISTS ONLY public."Usuario" DROP CONSTRAINT IF EXISTS "fk_facturaEn";
ALTER TABLE IF EXISTS ONLY public.descuenta DROP CONSTRAINT IF EXISTS fk_descuenta_evento;
ALTER TABLE IF EXISTS ONLY public.descuenta DROP CONSTRAINT IF EXISTS fk_descuenta_categoria;
ALTER TABLE IF EXISTS ONLY public.asiste DROP CONSTRAINT IF EXISTS fk_asiste_tarjeta;
ALTER TABLE IF EXISTS ONLY public.asiste DROP CONSTRAINT IF EXISTS fk_asiste_evento;
ALTER TABLE IF EXISTS ONLY public."descuentaAtraccion" DROP CONSTRAINT IF EXISTS "descuentaAtraccion_idAtraccion_fkey";
ALTER TABLE IF EXISTS ONLY public."descuentaAtraccion" DROP CONSTRAINT IF EXISTS "descuentaAtraccion_codigoCategoria_fkey";
ALTER TABLE IF EXISTS ONLY public."EventoEspecial" DROP CONSTRAINT IF EXISTS "EventoEspecial_idEvento_fkey";
ALTER TABLE IF EXISTS ONLY public."Atraccion" DROP CONSTRAINT IF EXISTS "Atraccion_idEvento_fkey";
DROP TRIGGER IF EXISTS trigger_check_tipo_evento_especial ON public."EventoEspecial";
DROP TRIGGER IF EXISTS trigger_check_sucedio_parque ON public."registraAtraccion";
DROP TRIGGER IF EXISTS trigger_check_rango_dias ON public.sucede;
DROP TRIGGER IF EXISTS trigger_check_dia_sucedio ON public.asiste;
DROP TRIGGER IF EXISTS check_medio_de_pago ON public."Pago";
DROP TRIGGER IF EXISTS atraccion_check_evento_tipo ON public."Atraccion";
ALTER TABLE IF EXISTS ONLY public."Usuario" DROP CONSTRAINT IF EXISTS unique_direccion;
ALTER TABLE IF EXISTS ONLY public."tieneCategoria" DROP CONSTRAINT IF EXISTS "tieneCategoria_pkey";
ALTER TABLE IF EXISTS ONLY public.sucede DROP CONSTRAINT IF EXISTS sucede_pkey;
ALTER TABLE IF EXISTS ONLY public."registraMedioDePago" DROP CONSTRAINT IF EXISTS "registraMedioDePago_pkey";
ALTER TABLE IF EXISTS ONLY public."registraAtraccion" DROP CONSTRAINT IF EXISTS "registraAtraccion_pkey";
ALTER TABLE IF EXISTS ONLY public.descuenta DROP CONSTRAINT IF EXISTS descuenta_pkey;
ALTER TABLE IF EXISTS ONLY public."descuentaAtraccion" DROP CONSTRAINT IF EXISTS "descuentaAtraccion_pkey";
ALTER TABLE IF EXISTS ONLY public.asiste DROP CONSTRAINT IF EXISTS asiste_pkey;
ALTER TABLE IF EXISTS ONLY public."Usuario" DROP CONSTRAINT IF EXISTS "Usuario_pkey";
ALTER TABLE IF EXISTS ONLY public."Telefono" DROP CONSTRAINT IF EXISTS "Telefono_pkey";
ALTER TABLE IF EXISTS ONLY public."Tarjeta" DROP CONSTRAINT IF EXISTS "Tarjeta_pkey";
ALTER TABLE IF EXISTS ONLY public."Provincia" DROP CONSTRAINT IF EXISTS "Provincia_pkey";
ALTER TABLE IF EXISTS ONLY public."Pais" DROP CONSTRAINT IF EXISTS "Pais_pkey";
ALTER TABLE IF EXISTS ONLY public."Pago" DROP CONSTRAINT IF EXISTS "Pago_pkey";
ALTER TABLE IF EXISTS ONLY public."MedioDePago" DROP CONSTRAINT IF EXISTS "MedioDePago_pkey";
ALTER TABLE IF EXISTS ONLY public."Factura" DROP CONSTRAINT IF EXISTS "Factura_pkey";
ALTER TABLE IF EXISTS ONLY public."Evento" DROP CONSTRAINT IF EXISTS "Evento_pkey";
ALTER TABLE IF EXISTS ONLY public."EventoEspecial" DROP CONSTRAINT IF EXISTS "EventoEspecial_pkey";
ALTER TABLE IF EXISTS ONLY public."Empresa" DROP CONSTRAINT IF EXISTS "Empresa_pkey";
ALTER TABLE IF EXISTS ONLY public."Direccion" DROP CONSTRAINT IF EXISTS "Direccion_pkey";
ALTER TABLE IF EXISTS ONLY public."Dia" DROP CONSTRAINT IF EXISTS "Dia_pkey";
ALTER TABLE IF EXISTS ONLY public."Categoria" DROP CONSTRAINT IF EXISTS "Categoria_pkey";
ALTER TABLE IF EXISTS ONLY public."Atraccion" DROP CONSTRAINT IF EXISTS "Atraccion_pkey";
ALTER TABLE IF EXISTS public."Telefono" ALTER COLUMN "idTelefono" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Tarjeta" ALTER COLUMN "numeroTarjeta" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Provincia" ALTER COLUMN "idProvincia" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Pais" ALTER COLUMN "idPais" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Pago" ALTER COLUMN "idPago" DROP DEFAULT;
ALTER TABLE IF EXISTS public."MedioDePago" ALTER COLUMN "idMedioDePago" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Factura" ALTER COLUMN "idFactura" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Evento" ALTER COLUMN "idEvento" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Direccion" ALTER COLUMN "idDireccion" DROP DEFAULT;
ALTER TABLE IF EXISTS public."Atraccion" ALTER COLUMN "idAtraccion" DROP DEFAULT;
DROP TABLE IF EXISTS public."tieneCategoria";
DROP TABLE IF EXISTS public.sucede;
DROP TABLE IF EXISTS public."registraMedioDePago";
DROP VIEW IF EXISTS public.facturas_adeudadas;
DROP VIEW IF EXISTS public.detalles_factura_atraccion;
DROP TABLE IF EXISTS public."registraAtraccion";
DROP VIEW IF EXISTS public.detalle_factura;
DROP TABLE IF EXISTS public.descuenta;
DROP VIEW IF EXISTS public.atracciones_con_descuento_por_categoria;
DROP TABLE IF EXISTS public."descuentaAtraccion";
DROP TABLE IF EXISTS public.asiste;
DROP TABLE IF EXISTS public."Usuario";
DROP SEQUENCE IF EXISTS public."Telefono_idTelefono_seq";
DROP TABLE IF EXISTS public."Telefono";
DROP SEQUENCE IF EXISTS public."Tarjeta_numeroTarjeta_seq";
DROP TABLE IF EXISTS public."Tarjeta";
DROP SEQUENCE IF EXISTS public."Provincia_idProvincia_seq";
DROP TABLE IF EXISTS public."Provincia";
DROP SEQUENCE IF EXISTS public."Pais_idPais_seq";
DROP TABLE IF EXISTS public."Pais";
DROP SEQUENCE IF EXISTS public."Pago_idPago_seq";
DROP TABLE IF EXISTS public."Pago";
DROP SEQUENCE IF EXISTS public."MedioDePago_idMedioDePago_seq";
DROP TABLE IF EXISTS public."MedioDePago";
DROP SEQUENCE IF EXISTS public."Factura_idFactura_seq";
DROP TABLE IF EXISTS public."Factura";
DROP SEQUENCE IF EXISTS public."Evento_idEvento_seq";
DROP TABLE IF EXISTS public."EventoEspecial";
DROP TABLE IF EXISTS public."Evento";
DROP TABLE IF EXISTS public."Empresa";
DROP SEQUENCE IF EXISTS public."Direccion_idDireccion_seq";
DROP TABLE IF EXISTS public."Direccion";
DROP TABLE IF EXISTS public."Dia";
DROP TABLE IF EXISTS public."Categoria";
DROP SEQUENCE IF EXISTS public."Atraccion_idAtraccion_seq";
DROP TABLE IF EXISTS public."Atraccion";
DROP FUNCTION IF EXISTS public.check_sucedio_parque();
DROP FUNCTION IF EXISTS public.check_sucede_dia();
DROP FUNCTION IF EXISTS public.check_rango_dias();
DROP FUNCTION IF EXISTS public.check_medio_de_pago();
DROP FUNCTION IF EXISTS public.check_evento_especial();
DROP FUNCTION IF EXISTS public.check_evento_atraccion();
DROP FUNCTION IF EXISTS public.cambios_de_categoria(dni integer, fecha1 date, fecha2 date);
DROP FUNCTION IF EXISTS public.atracion_mas_visitada_rango_fecha(fecha1 date, fecha2 date);
DROP EXTENSION IF EXISTS plpgsql;
DROP SCHEMA IF EXISTS public;
--
-- Name: DATABASE ecsa; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE ecsa IS 'Bases de datos del tp1';


--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: atracion_mas_visitada_rango_fecha(date, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.atracion_mas_visitada_rango_fecha(fecha1 date, fecha2 date) RETURNS TABLE(dni integer, "idAtraccion" integer)
    LANGUAGE sql
    AS $$ 
SELECT u.dni, r."idAtraccion" 
FROM "Usuario" u 
INNER JOIN "Tarjeta" t                  
    ON t."numeroTarjeta" = u."idTarjeta" 
INNER JOIN "registraAtraccion" r        
    ON r."numeroTarjeta" = t."numeroTarjeta" 
WHERE date(r.timestamp) > fecha1 and date(r.timestamp) < fecha2
GROUP BY u.dni, r."idAtraccion" 
HAVING 
COUNT(*) >= ALL (SELECT COUNT(*)
    FROM "registraAtraccion" r2 
        INNER JOIN "Tarjeta" t2 
            ON t2."numeroTarjeta" = r2."numeroTarjeta" 
        INNER JOIN "Usuario" u2 
            ON u2."idTarjeta" = t2."numeroTarjeta"
        WHERE date(r2.timestamp) > fecha1 and date(r2.timestamp) < fecha2
            GROUP BY u2.dni, r2."idAtraccion"
            HAVING u.dni = u2.dni) 
$$;


ALTER FUNCTION public.atracion_mas_visitada_rango_fecha(fecha1 date, fecha2 date) OWNER TO postgres;

--
-- Name: cambios_de_categoria(integer, date, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.cambios_de_categoria(dni integer, fecha1 date, fecha2 date) RETURNS TABLE(codigocategoria character, fecha date)
    LANGUAGE sql
    AS $$ SELECT c."codigoCategoria", c.fecha FROM "Usuario" u INNER JOIN "tieneCategoria" c ON c."numeroTarjeta" = u."idTarjeta" where u.dni = dni and c.fecha > fecha1 and c.fecha < fecha2 $$;


ALTER FUNCTION public.cambios_de_categoria(dni integer, fecha1 date, fecha2 date) OWNER TO postgres;

--
-- Name: check_evento_atraccion(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.check_evento_atraccion() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    IF EXISTS (SELECT 1
                FROM "Evento" e
                WHERE e."idEvento" = NEW."idEvento" AND
                e."tipo" != 'P') THEN
        RAISE EXCEPTION 'La nueva atraccion debe ser de un evento de tipo parque "P")';
    END IF;
    RETURN NEW;
END;
$$;


ALTER FUNCTION public.check_evento_atraccion() OWNER TO postgres;

--
-- Name: check_evento_especial(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.check_evento_especial() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF EXISTS (SELECT 1
                FROM "Evento" e
                WHERE e."idEvento" = NEW."idEvento" AND
                e."tipo" != 'E') THEN
        RAISE EXCEPTION 'La nueva atraccion debe ser de un evento especial ("E")';
    END IF;
    RETURN NEW;
END
$$;


ALTER FUNCTION public.check_evento_especial() OWNER TO postgres;

--
-- Name: check_medio_de_pago(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.check_medio_de_pago() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	IF NOT EXISTS(SELECT 1
				   FROM "registraMedioDePago" r, 
				   "Factura" f, 
				   "Tarjeta" t,
				   "Usuario" u
				   WHERE NEW."idFactura" = f."idFactura"
				   AND f."idTarjeta" = t."numeroTarjeta" AND 
				   u."idTarjeta" = t."numeroTarjeta" AND
				   u.dni = r.dni AND
				   r."idMedioDePago" = NEW."idMedioDePago") THEN
		RAISE EXCEPTION 'Todo pago debe ser con un medio de pago registrado por el usuario';
	END IF;
	RETURN NEW;
END
$$;


ALTER FUNCTION public.check_medio_de_pago() OWNER TO postgres;

--
-- Name: check_rango_dias(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.check_rango_dias() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    IF (SELECT s.fecha_desde   
        FROM "EventoEspecial" s
        WHERE "idEvento" = NEW."idEvento") > NEW.dia
        OR
        (SELECT s.fecha_hasta   
        FROM "EventoEspecial" s
        WHERE "idEvento" = NEW."idEvento") < NEW.dia THEN
            RAISE EXCEPTION 'Día invalido: Debe estar en el rango de fechas del evento';
    END IF;
    RETURN NEW;
END
$$;


ALTER FUNCTION public.check_rango_dias() OWNER TO postgres;

--
-- Name: check_sucede_dia(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.check_sucede_dia() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    IF NOT EXISTS(SELECT 1
                    FROM "sucede" s
                    WHERE
                      s."idEvento" = NEW."idEvento"
                      AND s."dia" = NEW."fecha") THEN
        RAISE EXCEPTION 'No se puede asistir a un dia que no sucedió';
    END IF;
    RETURN NEW;
END
$$;


ALTER FUNCTION public.check_sucede_dia() OWNER TO postgres;

--
-- Name: check_sucedio_parque(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.check_sucedio_parque() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	IF NOT EXISTS(SELECT 1
				    FROM "sucede" s
				    INNER JOIN "Atraccion" a
				    ON s."idEvento" = a."idEvento"
				    WHERE a."idAtraccion" = NEW."idAtraccion"
				           AND date(NEW.timestamp) = "dia") THEN
		RAISE EXCEPTION 'Para asistir a una atraccion, el parque de esta atraccion tiene que tener registrado el dia';
	END IF;
	RETURN NEW;
END
$$;


ALTER FUNCTION public.check_sucedio_parque() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Atraccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Atraccion" (
    "idAtraccion" integer NOT NULL,
    precio real NOT NULL,
    edad_desde integer NOT NULL,
    edad_hasta integer NOT NULL,
    altura_desde real NOT NULL,
    altura_hasta real NOT NULL,
    "idEvento" integer,
    CONSTRAINT chk_altura CHECK (((altura_desde > (0)::double precision) AND (altura_desde <= (altura_hasta)::double precision))),
    CONSTRAINT chk_edad CHECK (((edad_desde > 0) AND (edad_desde <= edad_hasta))),
    CONSTRAINT chk_precio CHECK ((precio >= (0)::double precision))
);


ALTER TABLE public."Atraccion" OWNER TO postgres;

--
-- Name: Atraccion_idAtraccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Atraccion_idAtraccion_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Atraccion_idAtraccion_seq" OWNER TO postgres;

--
-- Name: Atraccion_idAtraccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Atraccion_idAtraccion_seq" OWNED BY public."Atraccion"."idAtraccion";


--
-- Name: Categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Categoria" (
    "codigoCategoria" character(20) NOT NULL,
    gasto_minimo integer NOT NULL,
    CONSTRAINT chk_gasto_minimo_positivo CHECK ((gasto_minimo >= 0))
);


ALTER TABLE public."Categoria" OWNER TO postgres;

--
-- Name: Dia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Dia" (
    dia date NOT NULL
);


ALTER TABLE public."Dia" OWNER TO postgres;

--
-- Name: Direccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Direccion" (
    "idDireccion" integer NOT NULL,
    nombre character varying NOT NULL,
    "idProvincia" integer NOT NULL
);


ALTER TABLE public."Direccion" OWNER TO postgres;

--
-- Name: Direccion_idDireccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Direccion_idDireccion_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Direccion_idDireccion_seq" OWNER TO postgres;

--
-- Name: Direccion_idDireccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Direccion_idDireccion_seq" OWNED BY public."Direccion"."idDireccion";


--
-- Name: Empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Empresa" (
    cuit integer NOT NULL,
    razon_social character varying NOT NULL,
    "idDireccion" integer NOT NULL
);


ALTER TABLE public."Empresa" OWNER TO postgres;

--
-- Name: Evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Evento" (
    "idEvento" integer NOT NULL,
    nombre character varying NOT NULL,
    cuit integer NOT NULL,
    "idDireccion" integer NOT NULL,
    tipo character(25),
    CONSTRAINT "chk_TipoEvento" CHECK (((tipo = 'P'::bpchar) OR (tipo = 'E'::bpchar)))
);


ALTER TABLE public."Evento" OWNER TO postgres;

--
-- Name: EventoEspecial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."EventoEspecial" (
    "idEvento" integer NOT NULL,
    fecha_desde date NOT NULL,
    fecha_hasta date NOT NULL
);


ALTER TABLE public."EventoEspecial" OWNER TO postgres;

--
-- Name: Evento_idEvento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Evento_idEvento_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Evento_idEvento_seq" OWNER TO postgres;

--
-- Name: Evento_idEvento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Evento_idEvento_seq" OWNED BY public."Evento"."idEvento";


--
-- Name: Factura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Factura" (
    "idFactura" integer NOT NULL,
    "fechaCreacion" date NOT NULL,
    "fechaVencimiento" date NOT NULL,
    "idTarjeta" integer NOT NULL
);


ALTER TABLE public."Factura" OWNER TO postgres;

--
-- Name: Factura_idFactura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Factura_idFactura_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Factura_idFactura_seq" OWNER TO postgres;

--
-- Name: Factura_idFactura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Factura_idFactura_seq" OWNED BY public."Factura"."idFactura";


--
-- Name: MedioDePago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."MedioDePago" (
    "idMedioDePago" integer NOT NULL,
    tipo character(20) NOT NULL
);


ALTER TABLE public."MedioDePago" OWNER TO postgres;

--
-- Name: MedioDePago_idMedioDePago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."MedioDePago_idMedioDePago_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."MedioDePago_idMedioDePago_seq" OWNER TO postgres;

--
-- Name: MedioDePago_idMedioDePago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."MedioDePago_idMedioDePago_seq" OWNED BY public."MedioDePago"."idMedioDePago";


--
-- Name: Pago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Pago" (
    "idPago" integer NOT NULL,
    "idFactura" integer NOT NULL,
    fecha date NOT NULL,
    "idMedioDePago" integer NOT NULL
);


ALTER TABLE public."Pago" OWNER TO postgres;

--
-- Name: Pago_idPago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Pago_idPago_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Pago_idPago_seq" OWNER TO postgres;

--
-- Name: Pago_idPago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Pago_idPago_seq" OWNED BY public."Pago"."idPago";


--
-- Name: Pais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Pais" (
    "idPais" integer NOT NULL,
    nombre character varying NOT NULL
);


ALTER TABLE public."Pais" OWNER TO postgres;

--
-- Name: Pais_idPais_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Pais_idPais_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Pais_idPais_seq" OWNER TO postgres;

--
-- Name: Pais_idPais_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Pais_idPais_seq" OWNED BY public."Pais"."idPais";


--
-- Name: Provincia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Provincia" (
    "idProvincia" integer NOT NULL,
    nombre character varying NOT NULL,
    "idPais" integer NOT NULL
);


ALTER TABLE public."Provincia" OWNER TO postgres;

--
-- Name: Provincia_idProvincia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Provincia_idProvincia_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Provincia_idProvincia_seq" OWNER TO postgres;

--
-- Name: Provincia_idProvincia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Provincia_idProvincia_seq" OWNED BY public."Provincia"."idProvincia";


--
-- Name: Tarjeta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Tarjeta" (
    "numeroTarjeta" integer NOT NULL,
    estado boolean DEFAULT false NOT NULL
);


ALTER TABLE public."Tarjeta" OWNER TO postgres;

--
-- Name: Tarjeta_numeroTarjeta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Tarjeta_numeroTarjeta_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Tarjeta_numeroTarjeta_seq" OWNER TO postgres;

--
-- Name: Tarjeta_numeroTarjeta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Tarjeta_numeroTarjeta_seq" OWNED BY public."Tarjeta"."numeroTarjeta";


--
-- Name: Telefono; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Telefono" (
    "idTelefono" integer NOT NULL,
    numero integer NOT NULL,
    dni integer
);


ALTER TABLE public."Telefono" OWNER TO postgres;

--
-- Name: Telefono_idTelefono_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Telefono_idTelefono_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Telefono_idTelefono_seq" OWNER TO postgres;

--
-- Name: Telefono_idTelefono_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Telefono_idTelefono_seq" OWNED BY public."Telefono"."idTelefono";


--
-- Name: Usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Usuario" (
    dni integer NOT NULL,
    nombre character varying NOT NULL,
    apellido character varying NOT NULL,
    foto bytea,
    "idTarjeta" integer NOT NULL,
    "idDireccion" integer NOT NULL
);


ALTER TABLE public."Usuario" OWNER TO postgres;

--
-- Name: asiste; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.asiste (
    "numeroTarjeta" integer NOT NULL,
    "idEvento" integer NOT NULL,
    fecha date NOT NULL,
    precio real NOT NULL
);


ALTER TABLE public.asiste OWNER TO postgres;

--
-- Name: descuentaAtraccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."descuentaAtraccion" (
    "codigoCategoria" character(10) NOT NULL,
    "idAtraccion" integer NOT NULL,
    descuento real NOT NULL,
    CONSTRAINT "Descuento es un porcentaje" CHECK (((descuento <= (100)::double precision) AND (descuento >= (0)::double precision)))
);


ALTER TABLE public."descuentaAtraccion" OWNER TO postgres;

--
-- Name: atracciones_con_descuento_por_categoria; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.atracciones_con_descuento_por_categoria AS
 SELECT "codigoCategoria",
    d."idAtraccion"
   FROM ((public."descuentaAtraccion" d
     JOIN public."Categoria" c USING ("codigoCategoria"))
     JOIN public."Atraccion" a USING ("idAtraccion"))
  WHERE (d.descuento > (0)::double precision);


ALTER TABLE public.atracciones_con_descuento_por_categoria OWNER TO postgres;

--
-- Name: descuenta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.descuenta (
    "codigoCategoria" character(1) NOT NULL,
    "idEvento" integer NOT NULL,
    precio real NOT NULL
);


ALTER TABLE public.descuenta OWNER TO postgres;

--
-- Name: detalle_factura; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.detalle_factura AS
 SELECT f."idFactura",
    a.fecha,
    e."idEvento",
    e.nombre,
    a.precio
   FROM ((public."Factura" f
     JOIN public.asiste a ON ((f."idTarjeta" = a."numeroTarjeta")))
     JOIN public."Evento" e USING ("idEvento"))
  WHERE ((a.fecha > f."fechaCreacion") AND (a.fecha < f."fechaVencimiento"));


ALTER TABLE public.detalle_factura OWNER TO postgres;

--
-- Name: registraAtraccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."registraAtraccion" (
    "numeroTarjeta" integer NOT NULL,
    "idAtraccion" integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);


ALTER TABLE public."registraAtraccion" OWNER TO postgres;

--
-- Name: detalles_factura_atraccion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.detalles_factura_atraccion AS
 SELECT f."idFactura",
    r."timestamp",
    r."idAtraccion"
   FROM (public."Factura" f
     JOIN public."registraAtraccion" r ON ((f."idTarjeta" = r."numeroTarjeta")))
  WHERE ((date(r."timestamp") >= f."fechaCreacion") AND (date(r."timestamp") <= f."fechaVencimiento"));


ALTER TABLE public.detalles_factura_atraccion OWNER TO postgres;

--
-- Name: facturas_adeudadas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.facturas_adeudadas AS
 SELECT f."idFactura"
   FROM public."Factura" f
  WHERE (NOT (EXISTS ( SELECT 1
           FROM public."Pago" p
          WHERE (f."idFactura" = p."idFactura"))));


ALTER TABLE public.facturas_adeudadas OWNER TO postgres;

--
-- Name: registraMedioDePago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."registraMedioDePago" (
    dni integer NOT NULL,
    "idMedioDePago" integer NOT NULL
);


ALTER TABLE public."registraMedioDePago" OWNER TO postgres;

--
-- Name: sucede; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sucede (
    dia date NOT NULL,
    "idEvento" integer NOT NULL,
    precio real NOT NULL
);


ALTER TABLE public.sucede OWNER TO postgres;

--
-- Name: tieneCategoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."tieneCategoria" (
    "numeroTarjeta" integer NOT NULL,
    "codigoCategoria" character(1) NOT NULL,
    fecha date NOT NULL
);


ALTER TABLE public."tieneCategoria" OWNER TO postgres;

--
-- Name: Atraccion idAtraccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Atraccion" ALTER COLUMN "idAtraccion" SET DEFAULT nextval('public."Atraccion_idAtraccion_seq"'::regclass);


--
-- Name: Direccion idDireccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Direccion" ALTER COLUMN "idDireccion" SET DEFAULT nextval('public."Direccion_idDireccion_seq"'::regclass);


--
-- Name: Evento idEvento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Evento" ALTER COLUMN "idEvento" SET DEFAULT nextval('public."Evento_idEvento_seq"'::regclass);


--
-- Name: Factura idFactura; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Factura" ALTER COLUMN "idFactura" SET DEFAULT nextval('public."Factura_idFactura_seq"'::regclass);


--
-- Name: MedioDePago idMedioDePago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MedioDePago" ALTER COLUMN "idMedioDePago" SET DEFAULT nextval('public."MedioDePago_idMedioDePago_seq"'::regclass);


--
-- Name: Pago idPago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Pago" ALTER COLUMN "idPago" SET DEFAULT nextval('public."Pago_idPago_seq"'::regclass);


--
-- Name: Pais idPais; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Pais" ALTER COLUMN "idPais" SET DEFAULT nextval('public."Pais_idPais_seq"'::regclass);


--
-- Name: Provincia idProvincia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Provincia" ALTER COLUMN "idProvincia" SET DEFAULT nextval('public."Provincia_idProvincia_seq"'::regclass);


--
-- Name: Tarjeta numeroTarjeta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tarjeta" ALTER COLUMN "numeroTarjeta" SET DEFAULT nextval('public."Tarjeta_numeroTarjeta_seq"'::regclass);


--
-- Name: Telefono idTelefono; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telefono" ALTER COLUMN "idTelefono" SET DEFAULT nextval('public."Telefono_idTelefono_seq"'::regclass);


--
-- Name: Atraccion Atraccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Atraccion"
    ADD CONSTRAINT "Atraccion_pkey" PRIMARY KEY ("idAtraccion");


--
-- Name: Categoria Categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Categoria"
    ADD CONSTRAINT "Categoria_pkey" PRIMARY KEY ("codigoCategoria");


--
-- Name: Dia Dia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Dia"
    ADD CONSTRAINT "Dia_pkey" PRIMARY KEY (dia);


--
-- Name: Direccion Direccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Direccion"
    ADD CONSTRAINT "Direccion_pkey" PRIMARY KEY ("idDireccion");


--
-- Name: Empresa Empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Empresa"
    ADD CONSTRAINT "Empresa_pkey" PRIMARY KEY (cuit);


--
-- Name: EventoEspecial EventoEspecial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventoEspecial"
    ADD CONSTRAINT "EventoEspecial_pkey" PRIMARY KEY ("idEvento");


--
-- Name: Evento Evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Evento"
    ADD CONSTRAINT "Evento_pkey" PRIMARY KEY ("idEvento");


--
-- Name: Factura Factura_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Factura"
    ADD CONSTRAINT "Factura_pkey" PRIMARY KEY ("idFactura");


--
-- Name: MedioDePago MedioDePago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."MedioDePago"
    ADD CONSTRAINT "MedioDePago_pkey" PRIMARY KEY ("idMedioDePago");


--
-- Name: Pago Pago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Pago"
    ADD CONSTRAINT "Pago_pkey" PRIMARY KEY ("idPago");


--
-- Name: Pais Pais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Pais"
    ADD CONSTRAINT "Pais_pkey" PRIMARY KEY ("idPais");


--
-- Name: Provincia Provincia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Provincia"
    ADD CONSTRAINT "Provincia_pkey" PRIMARY KEY ("idProvincia");


--
-- Name: Tarjeta Tarjeta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tarjeta"
    ADD CONSTRAINT "Tarjeta_pkey" PRIMARY KEY ("numeroTarjeta");


--
-- Name: Telefono Telefono_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telefono"
    ADD CONSTRAINT "Telefono_pkey" PRIMARY KEY ("idTelefono");


--
-- Name: Usuario Usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario"
    ADD CONSTRAINT "Usuario_pkey" PRIMARY KEY (dni);


--
-- Name: asiste asiste_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asiste
    ADD CONSTRAINT asiste_pkey PRIMARY KEY ("numeroTarjeta", "idEvento", fecha);


--
-- Name: descuentaAtraccion descuentaAtraccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."descuentaAtraccion"
    ADD CONSTRAINT "descuentaAtraccion_pkey" PRIMARY KEY ("codigoCategoria", "idAtraccion");


--
-- Name: descuenta descuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.descuenta
    ADD CONSTRAINT descuenta_pkey PRIMARY KEY ("codigoCategoria", "idEvento");


--
-- Name: registraAtraccion registraAtraccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."registraAtraccion"
    ADD CONSTRAINT "registraAtraccion_pkey" PRIMARY KEY ("numeroTarjeta", "idAtraccion", "timestamp");


--
-- Name: registraMedioDePago registraMedioDePago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."registraMedioDePago"
    ADD CONSTRAINT "registraMedioDePago_pkey" PRIMARY KEY (dni, "idMedioDePago");


--
-- Name: sucede sucede_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sucede
    ADD CONSTRAINT sucede_pkey PRIMARY KEY (dia, "idEvento");


--
-- Name: tieneCategoria tieneCategoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tieneCategoria"
    ADD CONSTRAINT "tieneCategoria_pkey" PRIMARY KEY ("numeroTarjeta", "codigoCategoria", fecha);


--
-- Name: Usuario unique_direccion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario"
    ADD CONSTRAINT unique_direccion UNIQUE ("idDireccion");


--
-- Name: Atraccion atraccion_check_evento_tipo; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER atraccion_check_evento_tipo BEFORE INSERT OR UPDATE ON public."Atraccion" FOR EACH ROW EXECUTE PROCEDURE public.check_evento_atraccion();


--
-- Name: Pago check_medio_de_pago; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER check_medio_de_pago BEFORE INSERT OR UPDATE ON public."Pago" FOR EACH ROW EXECUTE PROCEDURE public.check_medio_de_pago();


--
-- Name: asiste trigger_check_dia_sucedio; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_check_dia_sucedio BEFORE INSERT OR UPDATE ON public.asiste FOR EACH ROW EXECUTE PROCEDURE public.check_sucede_dia();


--
-- Name: sucede trigger_check_rango_dias; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_check_rango_dias BEFORE INSERT OR UPDATE ON public.sucede FOR EACH ROW EXECUTE PROCEDURE public.check_rango_dias();


--
-- Name: registraAtraccion trigger_check_sucedio_parque; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_check_sucedio_parque BEFORE INSERT OR UPDATE ON public."registraAtraccion" FOR EACH ROW EXECUTE PROCEDURE public.check_sucedio_parque();


--
-- Name: EventoEspecial trigger_check_tipo_evento_especial; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_check_tipo_evento_especial BEFORE INSERT OR UPDATE ON public."EventoEspecial" FOR EACH ROW EXECUTE PROCEDURE public.check_evento_especial();


--
-- Name: Atraccion Atraccion_idEvento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Atraccion"
    ADD CONSTRAINT "Atraccion_idEvento_fkey" FOREIGN KEY ("idEvento") REFERENCES public."Evento"("idEvento") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: EventoEspecial EventoEspecial_idEvento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventoEspecial"
    ADD CONSTRAINT "EventoEspecial_idEvento_fkey" FOREIGN KEY ("idEvento") REFERENCES public."Evento"("idEvento") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: descuentaAtraccion descuentaAtraccion_codigoCategoria_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."descuentaAtraccion"
    ADD CONSTRAINT "descuentaAtraccion_codigoCategoria_fkey" FOREIGN KEY ("codigoCategoria") REFERENCES public."Categoria"("codigoCategoria") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: descuentaAtraccion descuentaAtraccion_idAtraccion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."descuentaAtraccion"
    ADD CONSTRAINT "descuentaAtraccion_idAtraccion_fkey" FOREIGN KEY ("idAtraccion") REFERENCES public."Atraccion"("idAtraccion") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: asiste fk_asiste_evento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asiste
    ADD CONSTRAINT fk_asiste_evento FOREIGN KEY ("idEvento") REFERENCES public."Evento"("idEvento") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: asiste fk_asiste_tarjeta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asiste
    ADD CONSTRAINT fk_asiste_tarjeta FOREIGN KEY ("numeroTarjeta") REFERENCES public."Tarjeta"("numeroTarjeta") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: descuenta fk_descuenta_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.descuenta
    ADD CONSTRAINT fk_descuenta_categoria FOREIGN KEY ("codigoCategoria") REFERENCES public."Categoria"("codigoCategoria") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: descuenta fk_descuenta_evento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.descuenta
    ADD CONSTRAINT fk_descuenta_evento FOREIGN KEY ("idEvento") REFERENCES public."Evento"("idEvento") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Usuario fk_facturaEn; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario"
    ADD CONSTRAINT "fk_facturaEn" FOREIGN KEY ("idDireccion") REFERENCES public."Direccion"("idDireccion") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Evento fk_organiza; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Evento"
    ADD CONSTRAINT fk_organiza FOREIGN KEY (cuit) REFERENCES public."Empresa"(cuit) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Pago fk_pago; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Pago"
    ADD CONSTRAINT fk_pago FOREIGN KEY ("idFactura") REFERENCES public."Factura"("idFactura") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Usuario fk_pertenece; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario"
    ADD CONSTRAINT fk_pertenece FOREIGN KEY ("idTarjeta") REFERENCES public."Tarjeta"("numeroTarjeta") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registraAtraccion fk_registra_atraccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."registraAtraccion"
    ADD CONSTRAINT fk_registra_atraccion FOREIGN KEY ("idAtraccion") REFERENCES public."Atraccion"("idAtraccion") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: registraMedioDePago fk_registra_medioDePago; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."registraMedioDePago"
    ADD CONSTRAINT "fk_registra_medioDePago" FOREIGN KEY ("idMedioDePago") REFERENCES public."MedioDePago"("idMedioDePago") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: registraAtraccion fk_registra_tarjeta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."registraAtraccion"
    ADD CONSTRAINT fk_registra_tarjeta FOREIGN KEY ("numeroTarjeta") REFERENCES public."Tarjeta"("numeroTarjeta") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registraMedioDePago fk_registra_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."registraMedioDePago"
    ADD CONSTRAINT fk_registra_usuario FOREIGN KEY (dni) REFERENCES public."Usuario"(dni) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sucede fk_sucede_dia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sucede
    ADD CONSTRAINT fk_sucede_dia FOREIGN KEY (dia) REFERENCES public."Dia"(dia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sucede fk_sucede_evento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sucede
    ADD CONSTRAINT fk_sucede_evento FOREIGN KEY ("idEvento") REFERENCES public."Evento"("idEvento") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Factura fk_tarjeta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Factura"
    ADD CONSTRAINT fk_tarjeta FOREIGN KEY ("idTarjeta") REFERENCES public."Tarjeta"("numeroTarjeta") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tieneCategoria fk_tiene_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tieneCategoria"
    ADD CONSTRAINT fk_tiene_categoria FOREIGN KEY ("codigoCategoria") REFERENCES public."Categoria"("codigoCategoria") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: tieneCategoria fk_tiene_tarjeta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tieneCategoria"
    ADD CONSTRAINT fk_tiene_tarjeta FOREIGN KEY ("numeroTarjeta") REFERENCES public."Tarjeta"("numeroTarjeta") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Empresa fk_ubicada; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Empresa"
    ADD CONSTRAINT fk_ubicada FOREIGN KEY ("idDireccion") REFERENCES public."Direccion"("idDireccion") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Provincia fk_ubicado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Provincia"
    ADD CONSTRAINT fk_ubicado FOREIGN KEY ("idPais") REFERENCES public."Pais"("idPais") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Direccion fk_ubicado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Direccion"
    ADD CONSTRAINT fk_ubicado FOREIGN KEY ("idProvincia") REFERENCES public."Provincia"("idProvincia") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Evento fk_ubicado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Evento"
    ADD CONSTRAINT fk_ubicado FOREIGN KEY ("idDireccion") REFERENCES public."Direccion"("idDireccion") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Pago fk_utiliza; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Pago"
    ADD CONSTRAINT fk_utiliza FOREIGN KEY ("idMedioDePago") REFERENCES public."MedioDePago"("idMedioDePago") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

