--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: Pais; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Pais" VALUES (1, 'Argentina');


--
-- Data for Name: Provincia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Provincia" VALUES (1, 'Buenos Aires', 1);
INSERT INTO public."Provincia" VALUES (2, 'Jujuy', 1);


--
-- Data for Name: Direccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Direccion" VALUES (1, 'Santo Tome 147', 1);
INSERT INTO public."Direccion" VALUES (2, 'Campana 123', 1);
INSERT INTO public."Direccion" VALUES (3, 'Concordia 457', 1);
INSERT INTO public."Direccion" VALUES (4, 'Guemes 187', 1);
INSERT INTO public."Direccion" VALUES (5, 'Acceso Norte 8784', 1);
INSERT INTO public."Direccion" VALUES (6, 'Aguirre, Victoria 154', 2);
INSERT INTO public."Direccion" VALUES (7, 'Ascasubi 7812', 2);
INSERT INTO public."Direccion" VALUES (8, 'Asunción 2448', 2);
INSERT INTO public."Direccion" VALUES (9, 'Albarellos 5451', 2);
INSERT INTO public."Direccion" VALUES (10, 'Arenales 8781', 2);
INSERT INTO public."Direccion" VALUES (11, 'Guatemala 1515', 1);
INSERT INTO public."Direccion" VALUES (12, 'Lavalleaj 123', 1);
INSERT INTO public."Direccion" VALUES (13, 'Pico 1515', 1);
INSERT INTO public."Direccion" VALUES (14, 'Remedios 2045', 1);
INSERT INTO public."Direccion" VALUES (15, 'Vigo 515', 2);
INSERT INTO public."Direccion" VALUES (16, 'Vidt 1515', 2);
INSERT INTO public."Direccion" VALUES (17, 'Vidt 874', 2);


--
-- Data for Name: Empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Empresa" VALUES (891218515, 'Empresa A', 6);
INSERT INTO public."Empresa" VALUES (518518181, 'Empresa B', 7);
INSERT INTO public."Empresa" VALUES (123456789, 'Empresa C', 8);
INSERT INTO public."Empresa" VALUES (481818481, 'Empresa E', 10);
INSERT INTO public."Empresa" VALUES (987654321, 'Diney Company', 9);


--
-- Data for Name: Evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Evento" VALUES (1, 'Parque Acasuso', 123456789, 11, 'P                        ');
INSERT INTO public."Evento" VALUES (3, 'Parque de la Costa', 481818481, 12, 'P                        ');
INSERT INTO public."Evento" VALUES (4, 'Parque Disney', 987654321, 13, 'P                        ');
INSERT INTO public."Evento" VALUES (5, 'Conferencia A', 518518181, 14, 'E                        ');
INSERT INTO public."Evento" VALUES (6, 'Conferencia B', 481818481, 15, 'E                        ');
INSERT INTO public."Evento" VALUES (7, 'Conferencia C', 123456789, 16, 'E                        ');
INSERT INTO public."Evento" VALUES (8, 'Conferencia B', 481818481, 15, 'E                        ');
INSERT INTO public."Evento" VALUES (9, 'Conferencia D', 481818481, 15, 'E                        ');
INSERT INTO public."Evento" VALUES (10, 'Conferencia C', 123456789, 15, 'E                        ');


--
-- Data for Name: Atraccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Atraccion" VALUES (8, 100, 3, 8, 1, 1.5, 1);
INSERT INTO public."Atraccion" VALUES (9, 100, 3, 18, 1, 1.5, 3);
INSERT INTO public."Atraccion" VALUES (1, 1000, 20, 80, 1, 2, 1);
INSERT INTO public."Atraccion" VALUES (10, 500, 1, 10, 1, 2, 3);
INSERT INTO public."Atraccion" VALUES (13, 5000, 10, 50, 1, 2, 4);
INSERT INTO public."Atraccion" VALUES (14, 5000, 1, 2, 0.200000003, 0.5, 4);
INSERT INTO public."Atraccion" VALUES (20, 500, 1, 2, 2, 5, 4);
INSERT INTO public."Atraccion" VALUES (22, 500, 1, 2, 2, 5, 1);


--
-- Data for Name: Categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Categoria" VALUES ('normal              ', 0);
INSERT INTO public."Categoria" VALUES ('silver              ', 1000);
INSERT INTO public."Categoria" VALUES ('gold                ', 5000);


--
-- Data for Name: Dia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Dia" VALUES ('2018-01-01');
INSERT INTO public."Dia" VALUES ('2018-01-04');
INSERT INTO public."Dia" VALUES ('2018-01-02');
INSERT INTO public."Dia" VALUES ('2018-01-03');
INSERT INTO public."Dia" VALUES ('2018-01-05');
INSERT INTO public."Dia" VALUES ('2018-01-06');
INSERT INTO public."Dia" VALUES ('2018-01-07');
INSERT INTO public."Dia" VALUES ('2018-01-08');
INSERT INTO public."Dia" VALUES ('2018-01-09');
INSERT INTO public."Dia" VALUES ('2018-01-10');
INSERT INTO public."Dia" VALUES ('2018-01-11');
INSERT INTO public."Dia" VALUES ('2018-01-12');
INSERT INTO public."Dia" VALUES ('2018-01-13');
INSERT INTO public."Dia" VALUES ('2017-12-29');
INSERT INTO public."Dia" VALUES ('2017-12-30');


--
-- Data for Name: EventoEspecial; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."EventoEspecial" VALUES (5, '2018-01-01', '2018-01-05');
INSERT INTO public."EventoEspecial" VALUES (6, '2018-01-01', '2018-01-05');
INSERT INTO public."EventoEspecial" VALUES (7, '2018-01-10', '2018-01-13');
INSERT INTO public."EventoEspecial" VALUES (8, '2018-01-07', '2018-01-09');
INSERT INTO public."EventoEspecial" VALUES (9, '2018-01-01', '2018-01-12');
INSERT INTO public."EventoEspecial" VALUES (10, '2017-12-30', '2017-12-31');


--
-- Data for Name: Tarjeta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Tarjeta" VALUES (1, true);
INSERT INTO public."Tarjeta" VALUES (2, true);
INSERT INTO public."Tarjeta" VALUES (3, true);
INSERT INTO public."Tarjeta" VALUES (5, true);
INSERT INTO public."Tarjeta" VALUES (4, true);
INSERT INTO public."Tarjeta" VALUES (6, true);
INSERT INTO public."Tarjeta" VALUES (7, true);


--
-- Data for Name: Factura; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Factura" VALUES (1, '2017-12-01', '2018-01-30', 1);


--
-- Data for Name: MedioDePago; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."MedioDePago" VALUES (1, 'Tarjeta Crédito     ');
INSERT INTO public."MedioDePago" VALUES (2, 'Tarjeta Debito      ');
INSERT INTO public."MedioDePago" VALUES (3, 'Paypal              ');
INSERT INTO public."MedioDePago" VALUES (4, 'Bitcoin             ');
INSERT INTO public."MedioDePago" VALUES (5, 'Patacones           ');
INSERT INTO public."MedioDePago" VALUES (6, 'Efectivo            ');


--
-- Data for Name: Pago; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: Telefono; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Telefono" VALUES (1, 47474747, 11111111);
INSERT INTO public."Telefono" VALUES (2, 48484848, 11111111);
INSERT INTO public."Telefono" VALUES (3, 12547624, 22222222);
INSERT INTO public."Telefono" VALUES (4, 71648237, 33333333);


--
-- Data for Name: Usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Usuario" VALUES (11111111, 'Pepe', 'Sanchez', NULL, 1, 1);
INSERT INTO public."Usuario" VALUES (22222222, 'Critina', 'Álvarez', NULL, 2, 2);
INSERT INTO public."Usuario" VALUES (33333333, 'Alfonso', 'Garrote', NULL, 3, 3);
INSERT INTO public."Usuario" VALUES (44444444, 'William', 'Garrote', NULL, 4, 4);
INSERT INTO public."Usuario" VALUES (55555555, 'Francesca', 'Ritondo', NULL, 5, 5);


--
-- Data for Name: asiste; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.asiste VALUES (1, 5, '2018-01-05', 100);
INSERT INTO public.asiste VALUES (1, 10, '2017-12-30', 100);


--
-- Data for Name: descuenta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: registraAtraccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."registraAtraccion" VALUES (1, 1, '2018-01-05 00:00:02');
INSERT INTO public."registraAtraccion" VALUES (1, 9, '2018-01-05 00:00:03');


--
-- Data for Name: registraMedioDePago; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."registraMedioDePago" VALUES (11111111, 1);
INSERT INTO public."registraMedioDePago" VALUES (22222222, 1);
INSERT INTO public."registraMedioDePago" VALUES (22222222, 2);
INSERT INTO public."registraMedioDePago" VALUES (33333333, 3);
INSERT INTO public."registraMedioDePago" VALUES (33333333, 4);
INSERT INTO public."registraMedioDePago" VALUES (33333333, 5);
INSERT INTO public."registraMedioDePago" VALUES (44444444, 1);
INSERT INTO public."registraMedioDePago" VALUES (44444444, 2);
INSERT INTO public."registraMedioDePago" VALUES (44444444, 3);
INSERT INTO public."registraMedioDePago" VALUES (44444444, 4);


--
-- Data for Name: sucede; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sucede VALUES ('2018-01-05', 5, 100);
INSERT INTO public.sucede VALUES ('2017-12-30', 10, 100);
INSERT INTO public.sucede VALUES ('2018-01-05', 1, 100);
INSERT INTO public.sucede VALUES ('2018-01-05', 3, 100);


--
-- Data for Name: tieneCategoria; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: Atraccion_idAtraccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Atraccion_idAtraccion_seq"', 22, true);


--
-- Name: Direccion_idDireccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Direccion_idDireccion_seq"', 17, true);


--
-- Name: Evento_idEvento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Evento_idEvento_seq"', 9, true);


--
-- Name: Factura_idFactura_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Factura_idFactura_seq"', 1, true);


--
-- Name: MedioDePago_idMedioDePago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."MedioDePago_idMedioDePago_seq"', 6, true);


--
-- Name: Pago_idPago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Pago_idPago_seq"', 1, false);


--
-- Name: Pais_idPais_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Pais_idPais_seq"', 1, true);


--
-- Name: Provincia_idProvincia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Provincia_idProvincia_seq"', 3, true);


--
-- Name: Tarjeta_numeroTarjeta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Tarjeta_numeroTarjeta_seq"', 7, true);


--
-- Name: Telefono_idTelefono_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Telefono_idTelefono_seq"', 1, false);


--
-- PostgreSQL database dump complete
--

